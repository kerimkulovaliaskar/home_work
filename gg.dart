import 'dart:io';

void main() {
  print(
      '1) Хотите обменять другую валюту на сом ? \n2) Хотите обменять сом на другую валюту ?\n');

  var a = ['RUB 0,98', 'EUR 100.10', 'USD 83.56', 'KZT 0.1253'];

  var b = ['RUB 1.4', 'EUR 101.11', 'USD 84.45', 'KZT 0.2014'];

  String min = stdin.readLineSync()!;

  int man = int.parse(min);

  // ignore: unrelated_type_equality_checks
  if (man == 1) {
    print(a);
  } else if (man == 2) {
    print(b);
  } else {
    print('lol it is not part of code');
    exit(100);
  }

  print('Выберите валюту \nRUB \nEUR \nUSD \nKZT\n');

  String card = stdin.readLineSync()!;

  print('Введите сумму\n');

  String first = stdin.readLineSync()!;

  double firstNum = double.tryParse(first) ?? 0;

  // String option = stdin.readLineSync()!;

  double firstE = 0.98;
  double firstF = 100.10;
  double firstJ = 83.56;
  double firstG = 0.1253;

  double firstA = 1.4;
  double firstB = 101.11;
  double firstC = 84.45;
  double firstD = 0.2014;

  if (man == 1) {
    print(card);
    switch (card) {
      case 'RUB':
        print(firstNum / firstE);
        break;
      case 'EUR':
        print(firstNum / firstF);
        break;
      case 'USD':
        print(firstNum / firstJ);
        break;
      case 'KZT':
        print(firstNum / firstG);
        break;
      default:
        print('Смотрите какой олень');
        exit(100);
    }
  }

  if (man == 2) {
    print(card);
    switch (card) {
      case 'RUB':
        print(firstNum * firstA);
        break;
      case 'EUR':
        print(firstNum * firstB);
        break;
      case 'USD':
        print(firstNum * firstC);
        break;
      case 'KZT':
        print(firstNum * firstD);
        break;
      default:
        print('Смотрите какой олень');
        exit(100);
    }
  }
}
